/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la;

import javafx.application.Application;
import javafx.stage.Stage;
import la.gui.GUI;

/**
 * This is the main driver.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class LethalityAssessmentApp extends Application {

    @Override
    public void start(Stage stage) {
        GUI gui = new GUI();
        //set up gui
        gui.setStage(stage);
        gui.setup();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
