/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import static la.gui.GUI.IMAGE_WARNING;
import la.gui.MessageDialog;

/**
 * This class serves as the evaluation component of the application. It gets
 inputs from the lethality assessment check itemList, and produces
 evalucation messages. Note that this implementation is version 1(Weight).
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class Evaluation {

    //messages
    public static final String THRESHOLD_STATS = "The lethality has reached the threshold";
    public static final String THRESHOLD_MESSAGE = "Please call staff!";
    public static final String EVALUCATION = "Lethality Evalucation";

    //the total lethality
    double totalLethality;

    //the total value
    double totalValue;

    //total mutilplier
    double totalMultiplier;

    //constructor
    public Evaluation() {
        totalLethality = 0;
        totalValue = 0;
        totalMultiplier = 1;
    }

    //getter and setter
    public double getTotalLethality() {
        totalLethality = totalValue * totalMultiplier;
        //round to int
        try {
            totalLethality = round(totalLethality, 0);
        } catch (IllegalArgumentException e) {
            System.err.println("IllegalArgumentException: " + e.getMessage());
        }
        return totalLethality;
    }

    public double getTotalMultiplier() {
        return totalMultiplier;
    }

    public void incTotalMultiplier(double mul) {
        totalMultiplier *= mul;
    }

    public void decTotalMultiplier(double mul) {
        totalMultiplier /= mul;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void incTotalValue(double value) {
        totalValue += value;
    }

    public void decTotalValue(double value) {
        totalValue -= value;
    }

    //if it has reached the threshold, it might be good to give us a warning
    public boolean hasReachedTheshold(double threshold) {
        return getTotalLethality() >= threshold;
    }

    //show message dialog
    public boolean showMessage() {
        MessageDialog dialog = new MessageDialog();
        dialog.setTitle(EVALUCATION);
        dialog.setHeader(THRESHOLD_STATS);
        dialog.setMessage(THRESHOLD_MESSAGE);
        dialog.setImage(IMAGE_WARNING);
        dialog.showDialog();
        return false;
    }

    //helper method for rounding numbers
    private double round(double number, int decimalPlaces) throws IllegalArgumentException {
        if (decimalPlaces < 0) {
            throw new IllegalArgumentException();
        }
        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(decimalPlaces, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
