/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import static la.file.User.GAUGE_VISIABLE_NO;
import static la.file.User.GAUGE_VISIABLE_YES;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * This class will read files and import all file and user data.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class FileManager {

    public static final String DATA_FILE_PATH = "/Lethality assessment C4G.xlsx";
    public static final String USER_FILE_PATH = "/User.xlsx";

    //for lethality item
    LethalityItem item;
    ArrayList<LethalityItem> iList;
    //for user
    User user;
    ArrayList<User> uList;

    public FileManager() {
    }

    public ArrayList<User> getUserList() {
        return uList;
    }

    public ArrayList<LethalityItem> getItemList() {
        return iList;
    }

    //read the dataFile and import all the lethality items into an arraylist
    public void readDataFile() {
        iList = new ArrayList<>();
        try {
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(getClass().getResourceAsStream(DATA_FILE_PATH));

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();

                //for each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                //skip the header, and check avaliable rows
                if (row.getRowNum() > 1 && row.getCell(0) != null) {

                    //for each avaliable row, we creat a lethality item to hold contents
                    item = new LethalityItem();

                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        //read very cell, 1st cell(type)
                        if (cell.getColumnIndex() == 0) {
                            switch (cell.getCellType()) {

                                case Cell.CELL_TYPE_STRING:
                                    item.setType(cell.getStringCellValue());
                                    break;
                            }
                        }

                        //2nd cell(text)
                        if (cell.getColumnIndex() == 1) {
                            switch (cell.getCellType()) {

                                case Cell.CELL_TYPE_STRING:
                                    item.setText(cell.getStringCellValue());
                                    break;
                            }
                        }

                        //4th cell(value)
                        if (cell.getColumnIndex() == 3) {
                            switch (cell.getCellType()) {
                                case Cell.CELL_TYPE_NUMERIC:
                                    item.getValue().add(cell.getNumericCellValue());
                                    break;
                                case Cell.CELL_TYPE_STRING:
                                    ArrayList<Double> value = new ArrayList<>();
                                    String[] sValue = cell.getStringCellValue().split(",");

                                    for (String s : sValue) {
                                        value.add(Double.parseDouble(s));
                                    }
                                    item.getValue().addAll(value);
                                    break;
                            }
                        }
                        //5th cell(multiplier)
                        if (cell.getColumnIndex() == 4) {
                            switch (cell.getCellType()) {
                                case Cell.CELL_TYPE_NUMERIC:
                                    item.setMul(cell.getNumericCellValue());
                                    break;

                            }
                        }
                    }
                    iList.add(item);
                }
            }
        } catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }

    //read the User File and import all the user infomation into an arraylist
    public void readUserFile() {
        uList = new ArrayList<>();
        try  {
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(getClass().getResourceAsStream(USER_FILE_PATH));

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();

                //for each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                //skip the header, and check avaliable rows
                if (row.getRowNum() > 0 && row.getCell(0) != null) {

                    //for each avaliable row, we creat a user item to hold contents
                    user = new User();

                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        //read very cell, 1st cell(username)
                        if (cell.getColumnIndex() == 0) {
                            switch (cell.getCellType()) {

                                case Cell.CELL_TYPE_STRING:

                                    user.setUsername(cell.getStringCellValue());
                                    break;
                            }
                        }

                        //2nd cell(threshold)
                        if (cell.getColumnIndex() == 1) {
                            switch (cell.getCellType()) {

                                case Cell.CELL_TYPE_NUMERIC:

                                    user.setThreshold(cell.getNumericCellValue());
                                    break;
                            }
                        }

                        //3rd cell(visiability)
                        if (cell.getColumnIndex() == 2) {
                            switch (cell.getCellType()) {
                                case Cell.CELL_TYPE_STRING:
                                    //if it is YES
                                    if (cell.getStringCellValue().equalsIgnoreCase(GAUGE_VISIABLE_YES)) {

                                        user.setVisiable(true);
                                    }
                                    if (cell.getStringCellValue().equalsIgnoreCase(GAUGE_VISIABLE_NO)) {

                                        user.setVisiable(false);
                                    }
                                    break;
                            }
                        }
                    }
                    uList.add(user);
                }
            }
        } catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }

}
