/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.file;

import java.util.ArrayList;

/**
 * This class is the data model containing info of a lethality item(question).
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class LethalityItem {
    public static final String TYPE_CATEGORY = "C";
    public static final String TYPE_QUESTION = "Q";
    
    //Q or C content
    String text;
    
    //value
    ArrayList<Double> value;
    
    //multiplier
    double mul;
    
    //type
    String type;
    
    public LethalityItem(){
        text = null;
        value = new ArrayList<>();
        mul = 0;
        type = null;
    }
    
    //getter and setter
    public String getType(){
        return type;
    }
    public void setType(String type){
       this.type = type;
    }
    
    public String getText(){
        return text;
    }
    public void setText(String text){
       this.text = text;
    }
    
    public ArrayList<Double> getValue(){
        return value;
    }
    
    public double getMul(){
        return mul;
    }
    public void setMul(double mul){
       this.mul = mul;
    }
}