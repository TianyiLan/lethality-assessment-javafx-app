/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.file;

/**
 * This class is the data model containing info of a user.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class User {
    static final String GAUGE_VISIABLE_YES = "Yes";
    static final String GAUGE_VISIABLE_NO = "No";
    
    //username
    String username;
    
    //threshold
    double threshold;
    
    //whether the gauge is visiable or not
    boolean visiable;
    
    public User(){
        username = null;
        threshold = 0;
        visiable = false;
    }
    
    //getter and setter
    public String getUsername(){
        return username;
    }
    public void setUsername(String username){
       this.username = username;
    }
    
    public double getThreshold(){
        return threshold;
    }
    public void setThreshold(double threshold){
       this.threshold = threshold;
    } 
    
    public boolean getVisiable(){
        return visiable;
    }
    public void setVisiable(boolean visiable){
       this.visiable = visiable;
    } 
}
