/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.gui;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import la.file.FileManager;
import la.file.LethalityItem;
import la.file.User;

/**
 * This is the graphic user interface. This class will render all gui
 * components.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class GUI {

    //last names starting with A-Z
    public static final String AUTHOR_1 = "Christopher Andrzejczyk";
    public static final String AUTHOR_2 = "Erik Driscoll";
    public static final String AUTHOR_3 = "Tianyi Lan";
    public static final String AUTHOR_4 = "Thomas Perrodin";

    //app title
    public static final String APP_TITLE = "Lethality Assessment App";

    //images and icons
    public static final String IMAGE_RESPONSE = "/response.png";
    public static final String IMAGE_CREDIT = "/credit.png";
    public static final String IMAGE_LOGIN = "/login.png";
    public static final String IMAGE_WARNING = "/warning.png";
    public static final String IMAGE_QUESTION = "/question.png";
    public static final String IMAGE_CHECK = "/check.png";
    public static final String IMAGE_NOTE = "/note.png";
    public static final String ICON_HEART = "/heart.png";
    public static final String ICON_START = "/start.png";
    public static final String ICON_RESTART = "/restart.png";
    public static final String ICON_HELP = "/help.png";
    public static final String ICON_EXIT = "/exit.png";
    public static final String ICON_USER = "/user.png";
    public static final String ICON_SNAPSHOT = "/snapshot.png";
    public static final String ICON_WRITE_A_NOTE = "/writeAnote.png";

    //tooltip
    public static final String TOOLTIP_START = "Start the Application";
    public static final String TOOLTIP_RESTART = "Restart the Application";
    public static final String TOOLTIP_HELP = "Show Credit of the Application";
    public static final String TOOLTIP_EXIT = "Exit the Application";
    public static final String TOOLTIP_SHOW_BAR = "Toggle the Lethality Bar";
    public static final String TOOLTIP_USER = "Login with a Different Username";
    public static final String TOOLTIP_SNAPSHOT = "Take a Snapshot";
    public static final String TOOLTIP_WRITE_A_NOTE = "Write a Note";
    //file manager
    FileManager fm;
    ArrayList<LethalityItem> itemList;
    ArrayList<User> userList;

    //list view
    LethalityListView view;

    //main components
    Stage stage;
    Scene scene;
    BorderPane appPane;
    BorderPane gui;
    HBox toolbar;

    //components in toolbar
    Button exit;
    Button start;
    Button restart;
    Button help;
    Button user;
    Button snapshot;
    CheckBox showBar;

    //components in gui
    Pane listPane;

    //user info
    String username = "";
    double threshold = 0;
    boolean visibility = false;

    //constructor
    public GUI() {
    }

    //set the stage from the main class
    public void setStage(Stage primaryStage) {
        stage = primaryStage;
    }

    //setup and initialize window
    public void setup() {
        //a start page first         
        initStartPage();
        //set title
        stage.setTitle(APP_TITLE);

        //full screen, however it is resizable
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        //set size
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());

        //set icon
        stage.getIcons().add(new Image(getClass().getResourceAsStream(ICON_HEART)));

        //now show the stage
        scene = new Scene(appPane);
        stage.setScene(scene);
        stage.show();
    }

    //initalize start page
    public void initStartPage() {
        //initialize file manager
        fm = new FileManager();
        itemList = new ArrayList<>();
        userList = new ArrayList<>();
        //read data file
        fm.readDataFile();
        //import all data into the itemList
        itemList = fm.getItemList();
        //read user file
        fm.readUserFile();
        //import all user data into the userList
        userList = fm.getUserList();

        //we will set a toolbar on bottom, and our main gui on center
        appPane = new BorderPane();
        //start page
        VBox startPage = new VBox();
        //tool bar 
        toolbar = new HBox();

        //style start page
        startPage.setStyle("-fx-background-color: WHITE;");
        startPage.setAlignment(Pos.CENTER);
        startPage.setSpacing(10);

        //app title
        Label title = new Label(APP_TITLE);
        title.setFont(Font.font("Cambria", 90));
        title.setTextFill(Color.RED);

        //app image
        Image appImage = new Image(getClass().getResourceAsStream(IMAGE_RESPONSE));
        
        //Image appImage = new Image(IMAGE_PATH + IMAGE_RESPONSE);
        ImageView appIV = new ImageView(appImage);

        //start and exit button
        start = buildButton(ICON_START, TOOLTIP_START);
        exit = buildButton(ICON_EXIT, TOOLTIP_EXIT);

        //effect for text
        DropShadow ds = new DropShadow();
        ds.setOffsetX(5);
        ds.setOffsetY(5);
        ds.setColor(Color.GRAY);
        title.setEffect(ds);

        //add contents on start page  
        startPage.getChildren().addAll(appIV, title);

        //add start and exit button on toolbar
        toolbar.setStyle("-fx-background-color: WHITE");
        toolbar.setPadding(new Insets(5, 10, 5, 10));
        toolbar.setAlignment(Pos.CENTER);
        toolbar.setSpacing(10);
        toolbar.getChildren().addAll(start, exit);

        //add start page and toolbar on appPane
        appPane.setCenter(startPage);
        appPane.setBottom(toolbar);

        //click start button 
        start.setOnAction((ActionEvent e) -> {
            LoginDialog login = new LoginDialog();
            login.setUserList(userList);
            if (login.showDialog()) {
                username = login.getUsername();
                threshold = login.getUserThreshold();
                visibility = login.getUserVisibility();
                //layout main gui
                layoutGUI();
            }
        });
        //click exit button
        exit.setOnAction((ActionEvent e) -> {
            System.exit(0);
        });
    }

    //layout all gui componnets
    public void layoutGUI() {
        //toolbar components
        Label currentUser = new Label("Current User: " + username);
        currentUser.setFont(Font.font("Cambria", 30));
        currentUser.setStyle("-fx-text-fill: BLACK;");
        showBar = new CheckBox("Bar");
        showBar.setStyle("-fx-text-fill: BLACK;");
        showBar.setTooltip(new Tooltip(TOOLTIP_SHOW_BAR));
        showBar.setSelected(false);
        //if the user has visibility, then the user can see and use the checkbox
        showBar.setVisible(visibility);
        restart = buildButton(ICON_RESTART, TOOLTIP_RESTART);
        help = buildButton(ICON_HELP, TOOLTIP_HELP);
        exit = buildButton(ICON_EXIT, TOOLTIP_EXIT);
        user = buildButton(ICON_USER, TOOLTIP_USER);
        snapshot = buildButton(ICON_SNAPSHOT, TOOLTIP_SNAPSHOT);

        //toolbar
        toolbar.setStyle("-fx-background-color: #b3d4fc");
        toolbar.setPadding(new Insets(5, 10, 5, 10));
        toolbar.setSpacing(10);
        toolbar.getChildren().clear();
        HBox pane = new HBox(currentUser);
        pane.setAlignment(Pos.CENTER_LEFT);
        toolbar.getChildren().addAll(pane, showBar, restart, user, snapshot, help, exit);
        HBox.setHgrow(pane, Priority.ALWAYS);
        toolbar.setAlignment(Pos.CENTER_RIGHT);

        //now for gui
        view = new LethalityListView();
        view.setCurrentThreshold(threshold);
        gui = new BorderPane();
        appPane.setCenter(gui);
        listPane = new StackPane();
        listPane.getChildren().add(view.buildListView(itemList));

        gui.setCenter(listPane);

        //finally set up handlers for all controls
        setupHandlers();
    }

    //set up handlers for buutons
    public void setupHandlers() {

        //lambda expression
        //put a method as a parameter to another method
        //input: actionEvent (like a function), output to setonaction: the result that e produces
        exit.setOnAction((ActionEvent e) -> {
            YesNoDialog dialog = new YesNoDialog();
            if (dialog.showDialog("Exit the application?")) {
                System.exit(0);
            }
        });

        restart.setOnAction((ActionEvent e) -> {
            YesNoDialog dialog = new YesNoDialog();
            if (dialog.showDialog("Restart the application?")) {
                layoutGUI();
            }
        });

        help.setOnAction((ActionEvent e) -> {
            MessageDialog dialog = new MessageDialog();
            dialog.setTitle("Credit");
            dialog.setHeader(null);
            dialog.setMessage("This application is designed and developed by\n"
                    + AUTHOR_1 + "\n"
                    + AUTHOR_2 + "\n"
                    + AUTHOR_3 + "\n"
                    + AUTHOR_4 + "\n"
                    + "\nProgramming language: Java\n"
                    + "Version: 1.0\n"
                    + "Stony Brook University\n"
                    + "Spring 2016");
            dialog.setImage(IMAGE_CREDIT);
            dialog.showDialog();
        });

        user.setOnAction((ActionEvent e) -> {
            LoginDialog login = new LoginDialog();
            login.setUserList(userList);
            login.setCurrentUser(username);
            login.wantToChangeUser(true);
            if (login.showDialog()) {
                username = login.getUsername();
                threshold = login.getUserThreshold();
                visibility = login.getUserVisibility();
                layoutGUI();
            }
        });

        snapshot.setOnAction((ActionEvent e) -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save Snapshot");

            WritableImage image = appPane.snapshot(new SnapshotParameters(), null);

            File file = fileChooser.showSaveDialog(stage);
            if (file != null) {
                try {
                    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
                } catch (IOException ex) {
                    System.err.println("IOException: " + ex.getMessage());
                }
            }
        });
        
        showBar.setOnAction(e->{
            //check to show gauge
            if(showBar.isSelected()){
                gui.setLeft(view.getBarPane());
            }//uncheck to hide gauge
            else{
                gui.setLeft(null);
                
            }
        });
    }
    //helper method to build a button
    public Button buildButton(String image, String tooltip) {
        Button button = new Button();
        Image buttonImage = new Image(getClass().getResourceAsStream(image));
        button.setGraphic(new ImageView(buttonImage));
        button.setTooltip(new Tooltip(tooltip));
        return button;
    }
}
