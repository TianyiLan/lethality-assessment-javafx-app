/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.gui;

import javafx.beans.property.IntegerProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 * This class is the bar conponent showing the current lethality level.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class LethalityBar {
    
    private final Paint BAR_COLOR_LEVEL_1 = Color.valueOf("#15ff00");
    private final Paint BAR_COLOR_LEVEL_2 = Color.valueOf("#49ff00");
    private final Paint BAR_COLOR_LEVEL_3 = Color.valueOf("#6fff00");
    private final Paint BAR_COLOR_LEVEL_4 = Color.valueOf("#a3ff00");
    private final Paint BAR_COLOR_LEVEL_5 = Color.valueOf("#c9ff00");
    private final Paint BAR_COLOR_LEVEL_6 = Color.valueOf("#f0ff00");
    private final Paint BAR_COLOR_LEVEL_7 = Color.valueOf("#fff400");
    private final Paint BAR_COLOR_LEVEL_8 = Color.valueOf("#ffc100");
    private final Paint BAR_COLOR_LEVEL_9 = Color.valueOf("#ff8100");
    private final Paint BAR_COLOR_LEVEL_10 = Color.valueOf("#ff2700");
    private final Paint BAR_COLOR_LEVEL_11 = Color.valueOf("#ff0000");

    BorderPane barRootPane;
    StackPane numPane;
    VBox barPane;
    Label num;
    Rectangle bar;
    double defaultMaxHeight;
    double userThreshold;

    public LethalityBar() {
        //init bar
        barRootPane = new BorderPane();
        barRootPane.setMinWidth(60);
        barRootPane.setPadding(new Insets(5, 3, 0, 3));

        num = new Label();
        numPane = new StackPane(num);

        bar = new Rectangle();
        bar.setWidth(35);
        bar.setFill(Color.RED);
     
 
        barPane = new VBox(bar);
        barPane.setAlignment(Pos.BOTTOM_CENTER);
        barRootPane.setCenter(barPane);
        barRootPane.setTop(numPane);
    }

    public void bindBar(IntegerProperty lethality) {
        num.textProperty().bind(lethality.asString());
        barPane.heightProperty().addListener(e -> redraw());
    }
    
    public void reflectBarHeight(double h){
        bar.setHeight(h);
    }
    public void reflectBarColor(double currentLethality, double userThreshold){
        double ratio = currentLethality/userThreshold;
        if(ratio < 0.1){
            bar.setFill(BAR_COLOR_LEVEL_1);
        }
        if(ratio >= 0.1 && ratio < 0.2){
            bar.setFill(BAR_COLOR_LEVEL_2);
        }
        if(ratio >= 0.2 && ratio < 0.3){
            bar.setFill(BAR_COLOR_LEVEL_3);
        }
        if(ratio >= 0.3 && ratio < 0.4){
            bar.setFill(BAR_COLOR_LEVEL_4);
        }
        if(ratio >= 0.4 && ratio < 0.5){
            bar.setFill(BAR_COLOR_LEVEL_5);
        }
        if(ratio >= 0.5 && ratio < 0.6){
            bar.setFill(BAR_COLOR_LEVEL_6);
        }
        if(ratio >= 0.6 && ratio < 0.7){
            bar.setFill(BAR_COLOR_LEVEL_7);
        }
        if(ratio >= 0.7 && ratio < 0.8){
            bar.setFill(BAR_COLOR_LEVEL_8);
        }
        if(ratio >= 0.8 && ratio < 0.9){
            bar.setFill(BAR_COLOR_LEVEL_9);
        }
        if(ratio >= 0.9 && ratio < 1){
            bar.setFill(BAR_COLOR_LEVEL_10);
        }
        if(ratio >= 1){
            bar.setFill(BAR_COLOR_LEVEL_11);
        }
    }
    
    private void redraw(){
        double newHeight = (bar.getHeight() * barPane.getHeight() * 0.95) / defaultMaxHeight;
        defaultMaxHeight = barPane.getHeight() * 0.95;
        bar.setHeight(newHeight);
    }

    public BorderPane getBarRootPane() {
        return barRootPane;
    }
    
    public double getBarMaxHeight(){
        defaultMaxHeight = barPane.getHeight() * 0.95;
        return defaultMaxHeight;
    }
}
