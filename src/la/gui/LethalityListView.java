/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.gui;

import java.util.ArrayList;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import la.data.Evaluation;
import la.file.LethalityItem;
import static la.file.LethalityItem.TYPE_CATEGORY;
import static la.file.LethalityItem.TYPE_QUESTION;
import static la.gui.GUI.ICON_WRITE_A_NOTE;
import static la.gui.GUI.TOOLTIP_WRITE_A_NOTE;

/**
 * This class serves as a list view which represents the lethality assessment checklist.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class LethalityListView {

    private static final double QUESTION_WIDTH = 500;
    private static final double BUTTON_PANE_WIDTH = 300;

    ListView<HBox> view;
    Evaluation eva;
    IntegerProperty totalLethality;
    double barH;
    boolean warningSeen;
    double currentThreshold;
    LethalityBar bar;
    double maxPossibleLethality;

    public LethalityListView() {
        eva = new Evaluation();
        totalLethality = new SimpleIntegerProperty();
        barH = 0;
        warningSeen = false;
        currentThreshold = 0;
        bar = new LethalityBar();
    }

    public IntegerProperty getTotalLethality() {
        return totalLethality;
    }

    public void setCurrentThreshold(double num) {
        currentThreshold = num;
    }

    public ListView buildListView(ArrayList<LethalityItem> data) {
        view = new ListView<>();
        view.setEditable(false);
        view.setItems(buildCellList(data));
        calMaxLethality(data);
        return view;
    }

    //given a dataset, computer the max lethality it could reach
    private double calMaxLethality(ArrayList<LethalityItem> data) {
        double maxV = 0;
        double maxM = 1;
        for (LethalityItem item : data) {
            //make sure its  negative question
            if (item.getType().equals(TYPE_QUESTION) && item.getMul() >= 1) {
                //this way, for 5 rating question, it always gets highest value
                maxV += item.getValue().get(item.getValue().size() - 1);
                maxM *= item.getMul();
            }
        }
        maxPossibleLethality = maxV * maxM;
        return maxPossibleLethality;
    }

    private void reflectBar() {
        barH = eva.getTotalLethality() * bar.getBarMaxHeight() / maxPossibleLethality;
        bar.reflectBarHeight(barH);
        bar.reflectBarColor(eva.getTotalLethality(), currentThreshold);
    }

    //helper method to build a button
    private Button buildCellButton(String image, String tooltip) {
        Button button = new Button();
        Image buttonImage = new Image(getClass().getResourceAsStream(image));
        button.setGraphic(new ImageView(buttonImage));
        button.setTooltip(new Tooltip(tooltip));
        return button;
    }

    //if the lethality reaches the threshold, pop up a warning
    private void showWarning() {
        if (warningSeen == false) {
            if (eva.hasReachedTheshold(currentThreshold)) {
                warningSeen = true;
                eva.showMessage();
            }
        }
    }

    //helper inner class, create for each cell to track the changes of value
    private class ValueSet {

        double value;
        double mul;
        boolean change;

        //getter and setter
        private ValueSet() {
            change = false;
        }

        private double getValue() {
            return value;
        }

        private double getMul() {
            return mul;
        }

        private boolean getChange() {
            return change;
        }

        private void setValue(double v) {
            value = v;
        }

        private void setMul(double m) {
            mul = m;
        }

        private void setChange(boolean c) {
            change = c;
        }
    }

    public BorderPane getBarPane() {
        return bar.getBarRootPane();
    }

    //build an observable list contained all cells
    private ObservableList buildCellList(ArrayList<LethalityItem> data) {
        ArrayList<HBox> cellList = new ArrayList<>();

        bar.bindBar(totalLethality);
        //for the data file, build a cell for each row in data file
        for (LethalityItem item : data) {
            ValueSet set = new ValueSet();
            HBox hbox = new HBox();
            Label question = new Label("");
            ToggleGroup group = new ToggleGroup();
            HBox bh = new HBox();
            RadioButton b1 = new RadioButton("1");
            RadioButton b2 = new RadioButton("2");
            RadioButton b3 = new RadioButton("3");
            RadioButton b4 = new RadioButton("4");
            RadioButton b5 = new RadioButton("5");
            RadioButton bYes = new RadioButton("Yes");
            RadioButton bNo = new RadioButton("No");
            Button bNote = buildCellButton(ICON_WRITE_A_NOTE, TOOLTIP_WRITE_A_NOTE);
            Label text = new Label("Note: ");
            Label notes = new Label("");

            //if it is a category
            if (item.getType().equals(TYPE_CATEGORY)) {
                question.setText(item.getText());
                question.setFont(Font.font("Cambria", 23));
                question.setTextFill(Color.BLUE);
                hbox.getChildren().clear();
                hbox.getChildren().add(question);
                hbox.setAlignment(Pos.CENTER_LEFT);
                cellList.add(hbox);
            }
            //if it is a question
            if (item.getType().equals(TYPE_QUESTION)) {
                //if it is yes/no question
                if (item.getValue().size() == 1) {
                    hbox.setSpacing(20);
                    hbox.setAlignment(Pos.CENTER_LEFT);
                    //show question first
                    question.setMinWidth(QUESTION_WIDTH);
                    question.setText(item.getText());
                    question.setFont(Font.font(15));
                    question.setTextFill(Color.BLACK);
                    //show radio buttons
                    bNo.setUserData("no");
                    bYes.setUserData("yes");
                    bYes.setToggleGroup(group);
                    bNo.setToggleGroup(group);
                    bh.getChildren().clear();
                    bh.getChildren().addAll(bYes, bNo);
                    bh.setAlignment(Pos.CENTER_LEFT);
                    bh.setMinWidth(BUTTON_PANE_WIDTH);
                    //now put everything on hbox cell
                    hbox.getChildren().clear();
                    hbox.getChildren().addAll(question, bh, bNote, text, notes);
                    cellList.add(hbox);
                    //define radio buttons
                    group.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
                        if (group.getSelectedToggle() != null) {
                            //if yes is clicked
                            if (group.getSelectedToggle().getUserData().equals("yes")) {
                                set.setChange(true);
                                set.setValue(item.getValue().get(0));
                                set.setMul(item.getMul());
                                eva.incTotalValue(set.getValue());
                                eva.incTotalMultiplier(set.getMul());
                                //reflect totalLethality
                                totalLethality.set((int)eva.getTotalLethality());
                                //reflect bar height
                                reflectBar();
                                showWarning();
                            }
                            //if no is clicked
                            if (group.getSelectedToggle().getUserData().equals("no")) {
                                if (set.getChange()) {
                                    eva.decTotalValue(set.getValue());
                                    eva.decTotalMultiplier(set.getMul());
                                    set.setChange(false);
                                    //reflect totalLethality
                                    totalLethality.set((int)eva.getTotalLethality());
                                    //reflect bar height
                                    reflectBar();
                                }
                            }
                        }
                    });

                    //define note button
                    bNote.setOnAction((ActionEvent e) -> {
                        NoteInputDialog dialog = new NoteInputDialog();
                        dialog.showDialog();
                        notes.setText(dialog.getNote());
                    });
                }

                //if it is self-rating question(5 rating)
                if (item.getValue().size() == 5) {
                    hbox.setSpacing(20);
                    hbox.setAlignment(Pos.CENTER_LEFT);
                    //show question first
                    question.setMinWidth(QUESTION_WIDTH);
                    question.setText(item.getText());
                    question.setFont(Font.font(15));
                    question.setTextFill(Color.BLACK);
                    //show radio buttons
                    b1.setUserData("1");
                    b2.setUserData("2");
                    b3.setUserData("3");
                    b4.setUserData("4");
                    b5.setUserData("5");
                    b1.setToggleGroup(group);
                    b2.setToggleGroup(group);
                    b3.setToggleGroup(group);
                    b4.setToggleGroup(group);
                    b5.setToggleGroup(group);
                    bh.getChildren().clear();
                    bh.getChildren().addAll(b1, b2, b3, b4, b5);
                    bh.setAlignment(Pos.CENTER_LEFT);
                    bh.setMinWidth(BUTTON_PANE_WIDTH);
                    //now put everything on hbox cell
                    hbox.getChildren().clear();
                    hbox.getChildren().addAll(question, bh, bNote, text, notes);
                    cellList.add(hbox);
                    //define radio buttons
                    group.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
                        if (group.getSelectedToggle() != null) {
                            //if 1 is clicked
                            if (group.getSelectedToggle().getUserData().equals("1")) {
                                if (!set.getChange()) {
                                    set.setChange(true);
                                    set.setValue(item.getValue().get(0));
                                    set.setMul(item.getMul());
                                    eva.incTotalValue(set.getValue());
                                    eva.incTotalMultiplier(set.getMul());
                                    //reflect totalLethality
                                    totalLethality.set((int)eva.getTotalLethality());
                                    //reflect bar height
                                    reflectBar();
                                    showWarning();
                                } else {
                                    //first dec value and mul
                                    eva.decTotalValue(set.getValue());
                                    eva.decTotalMultiplier(set.getMul());
                                    //then inc new value and mul
                                    set.setValue(item.getValue().get(0));
                                    set.setMul(item.getMul());
                                    eva.incTotalValue(set.getValue());
                                    eva.incTotalMultiplier(set.getMul());
                                    //reflect totalLethality
                                    totalLethality.set((int)eva.getTotalLethality());
                                    //reflect bar height
                                    reflectBar();
                                }
                            }
                            //if 2 is clicked
                            if (group.getSelectedToggle().getUserData().equals("2")) {
                                if (!set.getChange()) {
                                    set.setChange(true);
                                    set.setValue(item.getValue().get(1));
                                    set.setMul(item.getMul());
                                    eva.incTotalValue(set.getValue());
                                    eva.incTotalMultiplier(set.getMul());
                                    //reflect totalLethality
                                    totalLethality.set((int)eva.getTotalLethality());
                                    //reflect bar height
                                    reflectBar();
                                    showWarning();
                                } else {
                                    //first dec value and mul
                                    eva.decTotalValue(set.getValue());
                                    eva.decTotalMultiplier(set.getMul());
                                    //then inc new value and mul
                                    set.setValue(item.getValue().get(1));
                                    set.setMul(item.getMul());
                                    eva.incTotalValue(set.getValue());
                                    eva.incTotalMultiplier(set.getMul());
                                    //reflect totalLethality
                                    totalLethality.set((int)eva.getTotalLethality());
                                    //reflect bar height
                                    reflectBar();
                                }
                            }
                            //if 3 is clicked
                            if (group.getSelectedToggle().getUserData().equals("3")) {
                                if (!set.getChange()) {
                                    set.setChange(true);
                                    set.setValue(item.getValue().get(2));
                                    set.setMul(item.getMul());
                                    eva.incTotalValue(set.getValue());
                                    eva.incTotalMultiplier(set.getMul());
                                    //reflect totalLethality
                                    totalLethality.set((int)eva.getTotalLethality());
                                    //reflect bar height
                                    reflectBar();
                                    showWarning();
                                } else {
                                    //first dec value and mul
                                    eva.decTotalValue(set.getValue());
                                    eva.decTotalMultiplier(set.getMul());
                                    //then inc new value and mul
                                    set.setValue(item.getValue().get(2));
                                    set.setMul(item.getMul());
                                    eva.incTotalValue(set.getValue());
                                    eva.incTotalMultiplier(set.getMul());
                                    //reflect totalLethality
                                    totalLethality.set((int)eva.getTotalLethality());
                                    //reflect bar height
                                    reflectBar();
                                }
                            }
                            //if 4 is clicked
                            if (group.getSelectedToggle().getUserData().equals("4")) {
                                if (!set.getChange()) {
                                    set.setChange(true);
                                    set.setValue(item.getValue().get(3));
                                    set.setMul(item.getMul());
                                    eva.incTotalValue(set.getValue());
                                    eva.incTotalMultiplier(set.getMul());
                                    //reflect totalLethality
                                    totalLethality.set((int)eva.getTotalLethality());
                                    //reflect bar height
                                    reflectBar();
                                    showWarning();
                                } else {
                                    //first dec value and mul
                                    eva.decTotalValue(set.getValue());
                                    eva.decTotalMultiplier(set.getMul());
                                    //then inc new value and mul
                                    set.setValue(item.getValue().get(3));
                                    set.setMul(item.getMul());
                                    eva.incTotalValue(set.getValue());
                                    eva.incTotalMultiplier(set.getMul());
                                    //reflect totalLethality
                                    totalLethality.set((int)eva.getTotalLethality());
                                    //reflect bar height
                                    reflectBar();
                                }
                            }
                        }
                        //if 5 is clicked
                        if (group.getSelectedToggle().getUserData().equals("5")) {
                            if (!set.getChange()) {
                                set.setChange(true);
                                set.setValue(item.getValue().get(4));
                                set.setMul(item.getMul());
                                eva.incTotalValue(set.getValue());
                                eva.incTotalMultiplier(set.getMul());
                                //reflect totalLethality
                                totalLethality.set((int)eva.getTotalLethality());
                                //reflect bar height
                                reflectBar();
                                showWarning();
                            } else {
                                //first dec value and mul
                                eva.decTotalValue(set.getValue());
                                eva.decTotalMultiplier(set.getMul());
                                //then inc new value and mul
                                set.setValue(item.getValue().get(4));
                                set.setMul(item.getMul());
                                eva.incTotalValue(set.getValue());
                                eva.incTotalMultiplier(set.getMul());
                                //reflect totalLethality
                                totalLethality.set((int)eva.getTotalLethality());
                                //reflect bar height
                                reflectBar();
                            }
                        }

                    });

                    //define note button
                    bNote.setOnAction((ActionEvent e) -> {
                        NoteInputDialog dialog = new NoteInputDialog();
                        dialog.showDialog();
                        notes.setText(dialog.getNote());
                    });
                }
            }
        }
        ObservableList<HBox> observableCellList = FXCollections.observableList(cellList);
        return observableCellList;
    }
}
