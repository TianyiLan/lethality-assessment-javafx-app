/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.gui;

import java.util.ArrayList;
import java.util.Optional;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import la.file.User;
import static la.gui.GUI.ICON_HEART;
import static la.gui.GUI.IMAGE_CHECK;
import static la.gui.GUI.IMAGE_LOGIN;


/**
 * This class is the login dialog model for user to login.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class LoginDialog {

    Dialog dialog;
    boolean isUsernameWrong;
    boolean isLogin;
    ArrayList<User> userList;
    String name;
    double threshold;
    boolean visibility;
    String currentUser;
    boolean wantToChangeUser;
    boolean isUsernameSame;

    public LoginDialog() {
        isUsernameWrong = false;
        isLogin = false;
        userList = new ArrayList<>();
        name = "";
        threshold = 0;
        visibility = false;
        currentUser = "none";
        wantToChangeUser = false;
        isUsernameSame = false;
    }

    public void setUserList(ArrayList<User> userList) {
        this.userList = userList;
    }

    //if change user
    public void wantToChangeUser(boolean yes){
        wantToChangeUser = yes;
    }
    
    //prevent the current user from logging in with the curent usename
    public void setCurrentUser(String user) {
        currentUser = user;
    }

    public String getUsername() {
        return name;
    }

    public double getUserThreshold() {
        return threshold;
    }

    public boolean getUserVisibility() {
        return visibility;
    }

    public boolean showDialog() {
        //the custom dialog
        dialog = new Dialog();
        dialog.setTitle("Login");
        dialog.setHeaderText("Please enter a username to login");

        //set icon
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream(ICON_HEART)));
        //set image
        Image image = new Image(getClass().getResourceAsStream(IMAGE_LOGIN));
        dialog.setGraphic(new ImageView(image));

        //set the button types.
        ButtonType login = new ButtonType("Login");
        ButtonType cancel = new ButtonType("Cancel");
        dialog.getDialogPane().getButtonTypes().addAll(login, cancel);

        //create the username label and field
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        HBox mPane = new HBox();
        Label message = new Label("Username does not exist, please try again   ");
        message.setVisible(false);
        message.setTextFill(Color.RED);
        //show message if username is wrong
        if (isUsernameWrong && !isUsernameSame) {
            message.setVisible(true);
        } 
        //show message if username is same
        if (!isUsernameWrong && isUsernameSame) {
            message.setText("Username already logged in, please try again");
            message.setVisible(true);
        } 
        
        mPane.getChildren().add(message);
        TextField username = new TextField();
        username.setPromptText("Username");

        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(mPane, 0, 1);
        grid.setColumnSpan(mPane, 3);
        dialog.getDialogPane().setContent(grid);

        //enable/fisable login button depending on whether a username was entered.
        Button loginButton = (Button) dialog.getDialogPane().lookupButton(login);
        loginButton.setDisable(true);

        //some validation
        username.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        //request focus on the username field by default.
        Platform.runLater(() -> username.requestFocus());

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.get() == cancel) {
            dialog.hide();
        }
        if (result.get() == login) {
            //if the current user tries to login with the same username
            if (wantToChangeUser == true && username.getText().equals(currentUser)) {
                  isUsernameSame = true;
                  isUsernameWrong = false;
                  //show message
                  showDialog();
            } else {
                //now check if username matches
                for (User e : userList) {
                    if (username.getText().equals(e.getUsername())) {
                        isLogin = true;
                        name = e.getUsername();
                        threshold = e.getThreshold();
                        visibility = e.getVisiable();
                    }
                }

                //logged in
                if (isLogin) {
                    MessageDialog check = new MessageDialog();
                    check.setTitle("Login Successfully");
                    check.setHeader(null);
                    check.setMessage("You have successfully logged in");
                    check.setImage(IMAGE_CHECK);
                    check.showDialog();
                } //login failed
                else {
                    isUsernameWrong = true;
                    isUsernameSame = false;
                    //show message
                    showDialog();
                }
            }
        }
        return isLogin;
    }
}
