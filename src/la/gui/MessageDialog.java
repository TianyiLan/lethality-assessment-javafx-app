/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.gui;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import static la.gui.GUI.ICON_HEART;

/**
 * This class is the message dialog model for displaying messages.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class MessageDialog {

    Alert dialog;

    public MessageDialog() {
        dialog = new Alert(Alert.AlertType.INFORMATION);
    }

    public void setMessage(String message) {
        dialog.setContentText(message);
    }

    public void setTitle(String title) {
        dialog.setTitle(title);
    }
    
    public void setHeader(String header) {
        dialog.setHeaderText(header);
    }

    public void setImage(String imageName) {
        //set icon for this dialog
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream(ICON_HEART)));
        //set image
        Image image = new Image(getClass().getResourceAsStream(imageName));
        dialog.setGraphic(new ImageView(image));
    }

    public void showDialog() {
        ButtonType ok = new ButtonType("Close");
        dialog.getButtonTypes().setAll(ok);
        Optional<ButtonType> result = dialog.showAndWait();
        if (result.get() == ok) {
            dialog.close();
        }
    }
}
