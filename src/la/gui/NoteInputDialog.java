/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.gui;

import java.util.Optional;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import static la.gui.GUI.ICON_HEART;
import static la.gui.GUI.IMAGE_NOTE;

/**
 * This class is the note-input dialog model for user to write a note for each question.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class NoteInputDialog {

    Dialog dialog;
    String notes;

    public NoteInputDialog() {
        dialog = new Dialog();
        notes = "";
    }
    
    public String getNote() {
        return notes;
    }

    public void showDialog() {
        //set icon for this dialog
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream(ICON_HEART)));
        //set image
        Image image = new Image(getClass().getResourceAsStream(IMAGE_NOTE));
        dialog.setGraphic(new ImageView(image));

        dialog.setTitle("Note");
        dialog.setHeaderText("Please enter a note");

        ButtonType ok = new ButtonType("OK");
        ButtonType cancel = new ButtonType("Cancel");
        dialog.getDialogPane().getButtonTypes().addAll(ok, cancel);

        //create the note label and field
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        //notes
        TextField note = new TextField();
        note.setMinWidth(180);
        note.setPromptText("Note");

        grid.add(new Label("Note:"), 0, 0);
        grid.add(note, 1, 0);
        dialog.getDialogPane().setContent(grid);

        //enable/fisable login button depending on whether a note was entered.
        Button loginButton = (Button) dialog.getDialogPane().lookupButton(ok);
        loginButton.setDisable(true);
        //some validation
        note.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        //request focus on the username field by default.
        Platform.runLater(() -> note.requestFocus());

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.get() == cancel) {
            dialog.hide();
        }
        if (result.get() == ok) {
            notes = note.getText();
        }
    }
}
