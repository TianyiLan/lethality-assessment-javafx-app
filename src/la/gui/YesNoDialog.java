/*
 * CSE 391 Project: Lethality Assessment
 * End User Agency: Response of Suffolk County
 * Professor: Tony Scarlatos
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package la.gui;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import static la.gui.GUI.ICON_HEART;
import static la.gui.GUI.IMAGE_QUESTION;

/**
 * This class is the confirmation dialog model for user to confirm the action.
 *
 * @author Christopher Andrzejczyk, Erik Driscoll, Tianyi Lan, Thomas Perrodin
 */
public class YesNoDialog {

    Alert dialog;
    boolean isUserSure;

    public YesNoDialog() {
        dialog = new Alert(AlertType.CONFIRMATION);
        isUserSure = false;
    }

    public boolean showDialog(String action) {
        //set icon for this dialog
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream(ICON_HEART)));
        //set image
        Image image = new Image(getClass().getResourceAsStream(IMAGE_QUESTION));
        dialog.setGraphic(new ImageView(image));

        dialog.setTitle("Confirmation");
        dialog.setHeaderText(null);
        dialog.setContentText(action);

        ButtonType no = new ButtonType("No");
        ButtonType yes = new ButtonType("Yes");
        dialog.getButtonTypes().setAll(yes, no);
        Optional<ButtonType> result = dialog.showAndWait();
        if (result.get() == no) {
            dialog.close();
        }
        if (result.get() == yes) {
            isUserSure = true;
        }
        return isUserSure;
    }
}
